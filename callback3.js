const fs = require('fs').promises;

const cardsOfListId = (givenId) => {
    return new Promise((resolve, reject) => {
        fs.readFile('../providedData/lists_1.json', 'utf8')
            .then((listContent) => {
                const listsData = JSON.parse(listContent);
                const listIdArray = [];

                Object.keys(listsData).forEach((listId) => {
                    if (listId === givenId) {
                        listsData[listId].forEach((innerObj) => {
                            listIdArray.push(innerObj.id);
                        });
                    }
                });

                fs.readFile('../providedData/cards_1.json', 'utf8')
                    .then((cardsContent) => {
                        const returnValue = {};
                        const cardsData = JSON.parse(cardsContent);

                        listIdArray.forEach((element) => {
                            Object.keys(cardsData).forEach((key) => {
                                if (key === element) {
                                    returnValue[key] = cardsData[key];
                                }
                            });
                        });

                        resolve(returnValue);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            })
            .catch((error) => {
                reject(error);
            });
    });
};

module.exports = cardsOfListId;
