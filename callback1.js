const fs = require('fs').promises


const boardInfo = (boardId) => {

    return new Promise((resolve, reject) => {

        const innerIdOfBoard = [];

        fs.readFile('../providedData/lists_1.json', 'utf8').then((content) => {

            const listData = JSON.parse(content)

            Object.keys(listData).forEach((key) => {

                if (key === boardId) {
                    listData[key].forEach((obj) => {
                        innerIdOfBoard.push(obj.id)
                    })
                }
            })

            fs.readFile('../providedData/cards_1.json', 'utf8').then((content) => {

                let resultObj = {}
                const cardsData = JSON.parse(content)

                innerIdOfBoard.forEach((id) => {

                    Object.keys(cardsData).forEach((key) => {

                        if (key === id) {
                            resultObj[key] = cardsData[key]

                        }

                    })
                })

                let countOfResultObj = Object.keys(resultObj)

                if (!countOfResultObj) {
                    reject(`Error occurred`)
                }
                else {
                    resolve(resultObj)
                }
            }).catch((error) => {
                console.log(error)
            });
        }).catch((error) => {
            console.log(error)
        })
    })

}

module.exports = boardInfo;