const fs = require('fs').promises;

let thanosId = 'mcu453ed';

function execute() {
    const thanosFetchId = () => {
        return fs.readFile('../providedData/lists_1.json', 'utf8')
            .then(content => {
                const jsonListData = JSON.parse(content);
                let thanosInnerId = [];

                Object.keys(jsonListData).forEach((obj) => {
                    if (obj === thanosId) {
                        jsonListData[obj].forEach((element) => {
                            thanosInnerId.push(element.id);
                        });
                    }
                });

                return thanosInnerId;
            });
    };

    const thanosInfo = (thanosInnerId) => {
        return fs.readFile('../providedData/cards_1.json', 'utf8')
            .then(content => {
                const jsonCardData = JSON.parse(content);

                console.log(`Information for Thanos boards:`);

                thanosInnerId.forEach(thanosId => {
                    Object.keys(jsonCardData).forEach((cardsId) => {
                        if (cardsId === thanosId)
                            console.log(jsonCardData[cardsId]);
                    });
                });

                return jsonCardData;
            });
    };

    const cardsForMindAndSpaceList = (jsonCardData) => {
        const MindId = 'qwsa221';
        const SpaceId = 'jwkh245';
        console.log(`All cards for the Mind and Space list are:`);

        Object.keys(jsonCardData).forEach((key) => {
            if (key === MindId || key === SpaceId)
                console.log(jsonCardData[key]);
        });
    };

    thanosFetchId()
        .then(thanosInnerId => thanosInfo(thanosInnerId))
        .then(jsonCardData => cardsForMindAndSpaceList(jsonCardData))
        .catch(error => console.error(error));
}

execute();
module.exports = execute;