const fs = require('fs').promises

const listOfBoardId = (boardId) => {

    return new Promise((resolve) => {
        fs.readFile('../providedData/lists_1.json', 'utf8').then((content) => {

            let returnValue = {}
            const listData = JSON.parse(content)

            Object.keys(listData).forEach((key) => {

                if (key === boardId)
                    returnValue[key] = listData[key]
            })
            resolve(returnValue)
        }).catch((error) => {
            console.log(error)
        })
    })
}

module.exports = listOfBoardId;